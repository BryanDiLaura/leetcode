/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    int minDepth(TreeNode* root) {
        
        //DFS - slow
//         if(!root) return 0;
//         if(!root->left && !root->right) return 1;
        
//         int r=INT_MAX, l=INT_MAX;
//         if(root->left) l = minDepth(root->left) +1;
//         if(root->right) r = minDepth(root->right) +1;
        
//         return min(l, r);
        
        //BFS - much faster
        if(!root) return 0;
        queue<TreeNode*> Q;
        Q.push(root);
        int i=0;
        while(i<1000000){
            ++i;
            int level = Q.size();
            for(int j=0; j<level; ++j){
                TreeNode* t = Q.front();
                Q.pop();
                if(t->left) Q.push(t->left);
                if(t->right) Q.push(t->right);
                if(!t->left && !t->right) return i;
            }
        }

        return -1;
    }
};