class Solution {
public:
    int maxProfit(vector<int>& prices) {
        int minP = INT_MAX;
        int profit = 0;
        for(const auto& p : prices){
            minP = min(minP, p);
            profit = max(profit, p-minP);
        }
        return profit;
        
        
        //(nlogn too slow)
//         int best = 0;
//         for(auto i = prices.begin(); i != prices.end(); ++i){
//             for(auto j = next(i); j != prices.end(); ++j){
//                 best = max(best, *j-*i);
//             }
//         }
        
//         return best;
    }
};