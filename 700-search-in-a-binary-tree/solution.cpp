/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* searchBST(TreeNode* root, int val) {
        //first solution recursive and bad
//         if(root == nullptr) return nullptr;
        
//         if(root->val == val) return root;
        
//         TreeNode* l = searchBST(root->left, val);
//         TreeNode* r = searchBST(root->right, val);
//         if(l) return l;
//         if(r) return r;
        
//         return nullptr;
        
//         if(root == nullptr) return nullptr;
        
        //second solution - faster
//         TreeNode* i = root;
//         while(i != nullptr){
//             if(i->val == val) return i;
//             else if(val < i->val) i = i->left;
//             else if(val > i->val) i = i->right;
//         }
        
//         return nullptr;
        
        //third solution - fastest
        while(root!=nullptr && root->val != val){
            val < root->val  ? root = root->left : root = root->right;
        }
        return root;
    }
};