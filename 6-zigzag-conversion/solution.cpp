#include <vector>
#include <string>

class Solution {
public:
    string convert(string s, int numRows) {
        if(numRows == 1) return s;
        
        vector<string> rows{};
        rows.resize(numRows);
        
        int curRow = 0;
        bool forward = false;
        
        for(char& c: s){
            rows[curRow] += c;
            
            if(curRow == numRows-1 || curRow == 0){
                forward = !forward;
            }
            
            if(forward){
                curRow++;
            }
            else{
                curRow--;
            }
        }
        
        string ret = "";
        for(auto row : rows){
            ret += row;
        }
        return ret;
    }
};