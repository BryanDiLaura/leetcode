template<size_t N>
constexpr array<int, N> allPowers(void){
    array<int,N> powers{0};
    for(auto i=0; i<N; ++i){
        powers[i] = pow(3,i);
    }
    return powers;
}

const auto AllPowers = allPowers<20>();

class Solution {
public:
    bool isPowerOfThree(int n) {
        
        for(const auto& p : AllPowers){
            if(n == p) return true;
        }
        return false;
    }
};