//second compile-time solution

#include <array>
#include <vector>

constexpr int factorial(const int N){
    int val = 0;
    for(int i=0; i<N; ++i){
        val += i;
    }
    return val;
}

constexpr int getIndex(const int row, const int index){
    return factorial(row) + index;
}

template<int N>
constexpr array<int, factorial(N) + N> makeTriangle(void){

    static_assert(N > 2, "N has to be > 2");

    array<int, factorial(N) + N> bigArr{};

    //fill in first two rows
    bigArr[getIndex(0,0)] = 1;
    bigArr[getIndex(1,0)] = 1;
    bigArr[getIndex(1,1)] = 1;

    //fill until we hit the desired size
    auto row = 2;
    while(row < N){
        bigArr[getIndex(row,0)] = 1;
        bigArr[getIndex(row,row)] = 1;

        for(auto i=1; i<row; ++i){
            bigArr[getIndex(row,i)] = bigArr[getIndex(row-1,i-1)] + bigArr[getIndex(row-1,i)];
        }

        row++;
    }

    return bigArr;    
}

static const auto statTriangle = makeTriangle<30>();

int runtimeGetVal(const int row, const int index){
    int location = 0;
    for(int i=0;i<row;++i){
        location += i;
    }
    return statTriangle[location + index];
}

class Solution {
public:
    vector<vector<int>> generate(int numRows){
    
        auto row =0;
        vector<vector<int>> triangle;

        for(auto i=0; i<numRows; ++i){
            vector<int> newRow;
            for(auto j=0; j<i+1; ++j){
                newRow.push_back(runtimeGetVal(i,j));
            }
            triangle.push_back(newRow);
        }

        return triangle;
    }
};