//first trivial solution

#include <vector>

class Solution {
public:
    vector<vector<int>> generate(int numRows){
    
        auto row =0;
        vector<vector<int>> triangle;

        while(row < numRows){
            if(row == 0){
                triangle.push_back({1});
            }
            else if(row == 1){
                triangle.push_back({1,1});
            }
            else{
                vector<int> newRow;
                newRow.resize(row+1);
                newRow[0] = 1;
                newRow[row] = 1;

                for(auto i=1; i<newRow.size()-1; ++i){
                    newRow[i] = triangle[row-1][i-1] + triangle[row-1][i];
                }
                triangle.push_back(newRow);
            }
            row++;
        }

        return triangle;
    }
};