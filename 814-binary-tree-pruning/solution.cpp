/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    TreeNode* pruneTree(TreeNode* root) {
        if(subTreeHas1(root)) return root;
        else return nullptr;
    }
    
    bool subTreeHas1(TreeNode* node){
        //leaf
        if(!node->left && !node->right){
            return node->val;
        }
        
        //recurse on children and prune as needed
        bool hasOne = false;
        if(node->left){
            if(subTreeHas1(node->left)) hasOne = true;
            else node->left = nullptr;
        }
        if(node->right){
            if(subTreeHas1(node->right)) hasOne = true;
            else node->right = nullptr;
        }
        
        //if we make it this far, we removed both sides. Return info up
        return hasOne || node->val;
    }
};