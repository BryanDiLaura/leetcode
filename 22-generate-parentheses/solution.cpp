class Solution {
public:
    vector<string> generateParenthesis(int n) {
        vector<string> ret; 
        genParen(ret, "", n, 0);
        return ret;
    }
    
    
    void genParen(vector<string>& vec, string s, int numOpen, int numClose){
        if(!numOpen && !numClose){
            vec.push_back(s);
        }
        
        if(numOpen) genParen(vec, s+"(", numOpen-1, numClose+1);
        if(numClose) genParen(vec, s+")", numOpen, numClose-1);
    }
};