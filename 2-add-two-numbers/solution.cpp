/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */


class Solution {
public:
    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
        ListNode* head = new ListNode(l1->val + l2->val);
        ListNode* index = head;
        ListNode* L1 = l1->next;
        ListNode* L2 = l2->next;
        
        int carry = 0;
        if(index->val >= 10){
            index->val -= 10;
            carry = 1;
        }
        
        while(L1 || L2){
            
            //figure out the value
            int value = carry;
            if(L1) value += L1->val;
            if(L2) value += L2->val;
            
            //handle carry
            if(value >= 10){
                carry = 1;
                value -= 10;
            }
            else{
                carry = 0;
            }
            
            //add the node
            ListNode* newNode = new ListNode(value);
            index->next = newNode;
            index = newNode;
            
            //move indicies
            if(L1) L1 = L1->next;
            if(L2) L2 = L2->next;
        }
        
        //handle leftover carry
        if(carry){
            ListNode* newNode = new ListNode(1);
            index->next = newNode;
        }
        
        return head;
    }
};