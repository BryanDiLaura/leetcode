

//note I was unable to get this one. There is still a subtle bug with how it sorts individual digits


bool sortingFunc(string lhs, string rhs){
    //go from left to right checking for differences
    auto lItr = lhs.begin();
    auto rItr = rhs.begin();

    auto maxValSeen = '0';

    while(lItr != lhs.end() && rItr != rhs.end()){
        if(*lItr != *rItr){
            return *lItr > *rItr;
        }

        maxValSeen = max(*lItr, maxValSeen);
        
        lItr++;
        rItr++;
    }

    //if we are here, the strings are either equal or different sizes

    //if they are the same, just return
    if (lhs.size() == rhs.size()) return true;

    //left is bigger. check for anything greater than what we've already seen
    if(lhs.size() > rhs.size()){
        while(lItr != lhs.end()){
            if(*lItr > maxValSeen){
                return true;
            }
            lItr++;
        }
        return false;
    }

    //right hand side
    else{
        while(rItr != rhs.end()){
            if(*rItr > maxValSeen){
                return false;
            }
            rItr++;
        }
        return true;
    }
}

class Solution {
public:
    string largestNumber(vector<int>& nums) {
        
        //handle corner case where all zeros
        bool allZero = true;
        for(auto c:nums){
            if(c!=0){
                allZero = false;
                break;
            }
        }
        if(allZero){
            return "0";
        }
    
        //we are convert numbers to strings to make them easier to work with digit-wise
        vector<vector<string>> strs;

        //breaking them into 9 groups based on the first digit
        strs.resize(10);
        for(auto &n:nums){
            auto s = to_string(n);
            int firstDigit = s.front() - '0';
            strs[firstDigit].push_back(s);
        }

        //now sort each list by going left to right
        for(auto &list: strs){
            sort(list.begin(), list.end(), sortingFunc);
        }

        //build the string
        string ret;
        for(auto i=strs.rbegin(); i!=strs.rend(); ++i){
            for(auto str: *i){
                ret += str;
            }
        }

        return ret;
    }
};