class Solution {
public:
    bool isPathCrossing(string path) {
        int x = 0;
        int y = 0;
        unordered_set<string> visited;
        visited.insert(makeCoord(0,0));
        
        for(const auto& c : path){
            switch(c){
                case 'N': y++; break;
                case 'S': y--; break;
                case 'E': x++; break;
                case 'W': x--; break;
            }
            
            string coord = makeCoord(x,y);
            if(visited.find(coord) != visited.end()){
                return true;
            }
            else{
                visited.insert(coord);
            }
        }
        
        return false;
    }
    
private:
    inline string makeCoord(int x, int y){
        return to_string(x) + "," + to_string(y);
    }
};