#include <vector>
#include <string>
#include <cmath>
#include <cctype>

//-----------------------------------------------------------------------
enum CharType{
    NUMBER,
    CHARACTER,
};

//-----------------------------------------------------------------------
string genString(const vector<CharType>& tokens, const unsigned int combo, const string& s){
    string retStr = "";
    auto charNum =0;
    for(int i=tokens.size()-1; i>=0; i--){

        if(tokens[i] == NUMBER){
            retStr += s[i];
        }
        else{
            //use binary 0 or 1 to determine upper or lower
            if(((combo >> charNum) & (0x1)) == 0){
                retStr += tolower(s[i]);
            }
            else{
                retStr += toupper(s[i]);
            }
            charNum++;
        }
    }

    //built the string backwards so need to reverse (arguably faster than prepending each time)
    auto blah = string(retStr.rbegin(), retStr.rend());

    return blah;
}

class Solution {
public:
    vector<string> letterCasePermutation(string S) {
        vector<CharType> tokens;
        auto charCount = 0;
        for (auto c: S){
            if ((c<='z' && c>='a') || (c<='Z' && c>='A')){
                tokens.push_back(CHARACTER);
                charCount++;
            }
            else{
                tokens.push_back(NUMBER);
            }
        }

        vector<string> result;

        //count in binary to generate combinations...
        for(unsigned int i=0; i<pow(2,charCount); ++i){
            result.push_back(genString(tokens, i, S));
        }

        return result;
    }
};