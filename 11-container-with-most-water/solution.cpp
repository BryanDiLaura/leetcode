class Solution {
public:
    int maxArea(vector<int>& height) {
        int maxArea = 0;
        
        //start with maximum width
        auto left = height.begin();
        auto right = height.end()-1;
        
        while(left < right){
            //calculate the area
            int h = min(*left, *right);
            maxArea = max(maxArea, int(h*(right-left)));
            
            //move iterators to the next best position compared to current height
            while(*left <= h && left < right) left++;
            while(*right <= h && left < right) right--;
        }
        
        return maxArea;
    }
};