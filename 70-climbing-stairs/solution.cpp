class Solution {
public:
    int climbStairs(int n) {
        if(n == 1) return 1;
        if(n == 2) return 2;
        
        int twoBehind = 1;
        int oneBehind = 2;
        int current;
        
        for(int i=2; i<n; ++i){
            current = twoBehind + oneBehind;
            twoBehind = oneBehind;
            oneBehind = current;
        }
        
        return current;
    }
};