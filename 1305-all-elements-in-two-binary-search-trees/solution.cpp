//---------------------------------------------------------------------
void populateStack(TreeNode* node, stack<int>* theStack){
    
    if(!node) return;
    
    //push all elements greater than current node
    if(node->right){
        populateStack(node->right, theStack);
    }

    //put in our own 
    theStack->push(node->val);

    //push all elements smaller than current node
    if(node->left){
        populateStack(node->left, theStack);
    }

    return;
}


class Solution {
public:
    vector<int> getAllElements(TreeNode* root1, TreeNode* root2) {
        stack<int> stack1, stack2;
        populateStack(root1, &stack1);
        populateStack(root2, &stack2);

        vector<int> ret;
        while(!stack1.empty() && !stack2.empty()){
            if(stack1.top() < stack2.top()){
                ret.push_back(stack1.top());
                stack1.pop();
            }
            else{
                ret.push_back(stack2.top());
                stack2.pop();
            }
        }

        //get the leftovers
        if(!stack1.empty()){
            while(!stack1.empty()){
                ret.push_back(stack1.top());
                stack1.pop();
            }
        }
        else{
            while(!stack2.empty()){
                ret.push_back(stack2.top());
                stack2.pop();
            }
        }

        return ret;
    }
};