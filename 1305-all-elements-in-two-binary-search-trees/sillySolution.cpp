class Solution {
public:
    void traverse(TreeNode* root,  vector<int>& v )
    {
        if(root == NULL)
        {
            return;
        }
        
        traverse(root->left , v);
        
        v.push_back(root->val);
        
        traverse(root->right , v);
    }
    
    vector<int> getAllElements(TreeNode* root1, TreeNode* root2) {
        vector<int> v ;
        traverse(root1, v);
        traverse(root2, v);
        
        sort(v.begin() , v.end());
        
        return v;
        
    }
};