class Solution {
public:
    int hammingDistance(int x, int y) {
        int ham = x ^ y;
        
        int hamDistance = 0;
        for(int i=0; i<31; ++i){
            hamDistance += ((ham >> i) & 0x1);
        }
        return hamDistance;
    }
};