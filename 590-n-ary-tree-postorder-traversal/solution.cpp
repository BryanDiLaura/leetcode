/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

class Solution {
public:
    vector<int> postorder(Node* root) {
        //first recursive solution. slow?
//         if(!root) return vector<int>();
//         vector<int> ret;
//         for(const auto& c : root->children){
//             auto child = postorder(c);
//             ret.insert(ret.end(), child.begin(), child.end());
//         }
//         ret.push_back(root->val);
        
//         return ret;
        
        //faster?? iterative solution
        vector<int> ret;
        stack<Node*> s;
        s.push(root);
        
        while(!s.empty() && root){
            Node* temp = s.top(); s.pop();
            ret.insert(ret.begin(), temp->val);
            for(const auto& c : temp->children){
                s.push(c);
            }
        }
        return ret;
        
        
    }
};