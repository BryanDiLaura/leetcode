class Solution {
public:
    int countCharacters(vector<string>& words, string chars) {
        
        //first attempt, using std::includes
//         sort(chars.begin(), chars.end());
        
//         int total = 0;
//         for(auto& w : words){
//             sort(w.begin(), w.end());
//             if(includes(chars.begin(), chars.end(), w.begin(), w.end())) total += w.size();
//         }
//         return total;
        
        
        //second attempt, using letter arrays
        array<int,26> pool{0};
        for(const auto& c : chars){
            pool[c - 'a']++;
        }
        
        int total = 0;
        for(const auto& w : words){
            if(checkWord(w, pool)) total += w.size();
        }
        
        return total;
    }
    
private: 
    inline bool checkWord(const string word, array<int, 26> pool){
        for(const auto& c : word){
            pool[c - 'a']--;
            if(pool[c-'a'] < 0) return false;
        }
        return true;
    }
};