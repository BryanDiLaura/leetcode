#include <array>
#include <algorithm>

//https://leetcode.com/problems/binary-number-with-alternating-bits/

//compile-time generate the list of possible numbers
constexpr std::array<int,40> fillChecker(void){
    std::array<int, 40> mArr{0};
    int val = 2;
    int otherVal = 1;
    for(int i=0;i<32; i+=2){
        mArr[i] = val;
        mArr[i+1] = otherVal;
        val = val << 2;
        val |= 2;
        otherVal = otherVal << 2;
        otherVal |= 1;
    }
    return mArr;
}

class Solution {
public:
    bool hasAlternatingBits(int n) {
        const auto mChecker = fillChecker();
        //check to see if our number is in there
        if (std::find(mChecker.begin(), mChecker.end(), n) != mChecker.end()){
            return true;
        }
        else{
            return false;
        }
    }
};