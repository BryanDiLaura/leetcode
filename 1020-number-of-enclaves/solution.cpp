class Solution {
public:
    
    void dfs(vector<vector<int>>& A, int x, int y){
        //note order is important here because of short-cutting - don't want to access
        //if we will go outside array
        if(x < 0 || y < 0 || x == A.size() || y == A[x].size() || A[x][y] == 0) return;
        
        A[x][y] = 0;
        
        dfs(A,x-1,y);
        dfs(A,x+1,y);
        dfs(A,x,y+1);
        dfs(A,x,y-1);
    }
    
    int numEnclaves(vector<vector<int>>& A) {

        //go around edges and invalidate all land touching edges
        for(int x=0;x<A.size();++x){
            for(int y=0;y<A[x].size();++y){
                if(x*y==0 || x==A.size()-1 || y==A[x].size()-1){
                    dfs(A,x,y);
                }
            }
        }
        
        //count all the ones
        return std::accumulate(begin(A), end(A), 0, [](auto c, auto row){
            return c + std::accumulate(begin(row), end(row), 0);
        });
    }
    
    

    //-----------------------------------------------------------------------------------
    //-------I originally solved this by counting full enclaves, rather than units-------
    //----------------------------whoops-------------------------------------------------
    //-----------------------------------------------------------------------------------
//     typedef vector<vector<int>> arr2D;
    
//     int numEnclaves(arr2D& A) {

//         //construct a "visited" strucutre
//         arr2D checked;
//         checked.resize(A.size());
//         for(auto& row : checked){
//             row.resize(A[0].size());
//         }

//         int numEnclaves = 0;

//         for(int y=1; y<A.size()-1;++y){
//             for(int x=1; x<A[y].size()-1;++x){
//                 if(!checked[x][y] && A[x][y]){
//                     if(!checkNotEnclave(A, checked, x, y)) numEnclaves++;
//                 }
//             }
//         }

//         return numEnclaves;
//     }


//     bool checkNotEnclave(const arr2D& A, arr2D& checked, int x, int y){

//         //do bookkeeping
//         checked[x][y] = 1;

//         //we are on an edge, so not an enclave
//         if(x == 0 || x == A[0].size() || y == 0 || y == A.size()) return true;

//         //check recursively on all neighboring 1's
//         bool result = false;
//         if(A[x+1][y] && !checked[x+1][y]) result |= checkNotEnclave(A, checked, x+1, y);
//         if(A[x-1][y] && !checked[x-1][y]) result |= checkNotEnclave(A, checked, x-1, y);
//         if(A[x][y+1] && !checked[x][y+1]) result |= checkNotEnclave(A, checked, x, y+1);
//         if(A[x][y-1] && !checked[x][y-1]) result |= checkNotEnclave(A, checked, x, y-1);

//         return result;
//     }
};