class Solution {
public:
    int maximumProduct(vector<int>& nums) {
        sort(nums.begin(), nums.end());
        auto n = nums.size();
        
        auto positivesOnly = nums[n-1]*nums[n-2]*nums[n-3];
        auto includeNegatives = nums[0]*nums[1]*nums[n-1];
        
        if(positivesOnly >= includeNegatives) return positivesOnly;
        else return includeNegatives;
    }
};