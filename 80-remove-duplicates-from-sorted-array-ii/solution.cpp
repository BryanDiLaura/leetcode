class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        if(nums.size() < 3) return nums.size();
        auto first = nums.begin();
        auto second = next(first);
        auto third = next(second);
        
        while(third != nums.end()){
            if(*first == *second && *second == *third){
                nums.erase(third);
                third = next(second);
            }
            else{
                first = second;
                second  = third;
                third = next(second);
            }
        }
        
        return nums.size();



        //note: generalizable for any k duplicates or if erase operation not available:
        // int i = 0;
        // for (int n : nums)
        //     if (i < k || n > nums[i-k])
        //         nums[i++] = n;
        // return i;
    }
};