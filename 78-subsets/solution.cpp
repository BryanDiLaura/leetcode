class Solution {
public:
    
    vector<int> getSubFromNum(unsigned int n, const vector<int>& nums){
        vector<int> ret;
        int i = 0;
        while(n){
            if (n&1) ret.push_back(nums[i]);
            i++;
            n = n>>1;
        }
        return ret;
    }
    
    vector<vector<int>> subsets(vector<int>& nums) {
        vector<vector<int>> ret;
        auto size = nums.size();
        size = 1<<size;
        for(unsigned int i=0; i<size; ++i){
            ret.push_back(getSubFromNum(i, nums));
        }
        return ret;
    }
};