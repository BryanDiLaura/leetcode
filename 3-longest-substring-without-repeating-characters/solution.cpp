#include <unordered_map>
#include <vector>

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        unordered_map<char, unsigned int> window;
        auto max = 0;

        //need to keep track of iterators and throw away before certain point
        for (auto i =0; i<s.size(); ++i){

            auto c = s[i];

            auto loc = window.find(c);
            if(loc == window.end()){
                //not found in map, add
                window[c] = i;
                if (window.size() >= max){
                    max = window.size();
                }
            }
            else{
                //found, need to delete everything <= last matching index
                vector<char> eraseList;
                for (auto [key, val] : window){
                    if(val < loc->second){
                        eraseList.push_back(key);
                    }
                }
                for(auto c : eraseList){
                    window.erase(c);
                }

                //update value to current position
                window[c] = i;
            }
        }
        return max;
    }
};