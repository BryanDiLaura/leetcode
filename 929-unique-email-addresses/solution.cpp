class Solution {
public:
    inline string sanatize(const string& s){
    string ret = "";
    auto index = s.begin();
    while(*index != '@'){
        if(*index == '+'){
            while(*index != '@'){
                index++;
            }
            break;
        }

        if(*index != '.'){
            ret += *index;
        }
        index++;
    }

    while(index != s.end()){
        ret += *index;
        index++;
    }

    return ret;
}

int numUniqueEmails(vector<string>& emails) {
    unordered_set<string> emailMap;

    for(auto& e: emails){
        emailMap.insert(sanatize(e));
    }

    return emailMap.size();
}
};