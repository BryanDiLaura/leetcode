class Solution {
public:
    bool isToeplitzMatrix(vector<vector<int>>& matrix) {
        auto m = matrix.size();
        auto n = matrix[0].size();
        
        if(m == 1) return true;
        
        for(auto row=1; row<m; ++row){
            for(auto col=1; col<n; ++col){
                if(matrix[row][col] != matrix[row-1][col-1]) return false;
            }
        }
        
        return true;
    }
};