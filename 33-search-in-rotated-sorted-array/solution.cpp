#include <algorithm>

//https://leetcode.com/problems/search-in-rotated-sorted-array/

class Solution {
public:
    int search(vector<int>& nums, int target) {
        auto res = std::find(nums.begin(), nums.end(), target);
        if (res != nums.end()){
            return res - nums.begin();
        }
        else{
            return -1;
        }
        
    }
};