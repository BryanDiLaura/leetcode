class Solution {
public:
    string reformat(string s) {
        string letters;
        string numbers;
        
        for(const auto& c : s){
            if(c >= 'a' && c <= 'z'){
                letters.push_back(c);
            }
            else{
                numbers.push_back(c);
            }
        }

        if(abs(int(letters.size() - numbers.size())) > 1) return "";
        
        bool lettersTurn = true;
        if(letters.size() < numbers.size()) lettersTurn = false;
        
        string ret;
        
        int l=0;
        int n=0;
        while(l<letters.size() || n<numbers.size()){
            if(lettersTurn){
                ret.push_back(letters[l]);
                l++;
            }
            else{
                ret.push_back(numbers[n]);
                n++;
            }
            lettersTurn = !lettersTurn;
        }
        
        return ret;
        
    }
};