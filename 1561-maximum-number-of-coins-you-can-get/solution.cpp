class Solution {
public:
    int maxCoins(vector<int>& piles) {
        sort(piles.begin(), piles.end());
        auto numPiles = piles.size() / 3;
        
        if(numPiles == 1) return piles[1];
        
        int ret = 0;
        for(int i = piles.size()-2; i>=numPiles; i = i-2){
            ret += piles[i];
        }
        
        return ret;
    }
};