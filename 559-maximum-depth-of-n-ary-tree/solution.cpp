/*
// Definition for a Node.
class Node {
public:
    int val;
    vector<Node*> children;

    Node() {}

    Node(int _val) {
        val = _val;
    }

    Node(int _val, vector<Node*> _children) {
        val = _val;
        children = _children;
    }
};
*/

class Solution {
public:
    int maxDepth(Node* root) {
        if(root) return maxDepthRecurse(root, 1);
        else return 0;
    }
    
    int maxDepthRecurse(Node* node, int currentDepth){
        
        if(node->children.size() == 0){
            return currentDepth;
        }
        
        int maxDepth = 0;
        for(auto & n : node->children){
            maxDepth = max(maxDepthRecurse(n, currentDepth+1), maxDepth);
        }
        
        return maxDepth;
    }
};