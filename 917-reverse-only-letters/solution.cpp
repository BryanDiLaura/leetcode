class Solution {
public:
    string reverseOnlyLetters(string S) {
        auto front = S.begin();
        auto back = S.end();
        
        bool onlyAlpha = true;
        
        while(front < back){
            onlyAlpha = true;
            if(!isalpha(*front)){
                front++;
                onlyAlpha = false;
            }
            if(!isalpha(*back)){
                back--;
                onlyAlpha = false;
            }
            
            if(onlyAlpha){
                swap(*front, *back);
                front++;
                back--;
            }
        }
        
        return S;
    }
};