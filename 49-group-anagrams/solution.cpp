class Solution {
public:
    vector<vector<string>> groupAnagrams(vector<string>& strs) {
        unordered_map<string, vector<string> > bigMap;
        for(auto& s : strs){
            string t = s;
            sort(t.begin(), t.end());
            bigMap[t].push_back(s);
        }
        
        vector<vector<string>> anagrams;
        for(auto& pair : bigMap){
            anagrams.push_back(move(pair.second));
        }
        
        return anagrams;
    }
};