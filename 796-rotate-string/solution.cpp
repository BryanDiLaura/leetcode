class Solution {
public:
    bool rotateString(string A, string B) {
        //initial trivial solution
        // if(A.size() != B.size()) return false;
        // if(A.size() == 0) return true;
        
        // for(int i=0; i<A.size(); ++i){
        //     rotate(A.rbegin(), A.rbegin()+1, A.rend());
        //     if(A==B) return true;
        // }
        
        // return false;
        
        //trick fast solution
        if(A.size() != B.size()) return false;
        return (A+A).find(B) != string::npos;
    }
};