class Solution {
public:
    bool judgeCircle(string moves) {
        int X = 0;
        int Y = 0;
        
        for(const auto& m : moves){
            switch(m){
                case 'U': 
                    Y++;
                    break;
                case 'D':
                    Y--;
                    break;
                case 'R':
                    X++;
                    break;
                case 'L':
                    X--;
                    break;
            }
        }
        return !(X || Y);
    }
};